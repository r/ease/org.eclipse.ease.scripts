loadModule("/Unittest");

startTest("valid", "simple valid test")
assertTrue(true)
endTest()

startTest("invalid", "simple fail test")
assertTrue(false)
endTest()

startTest("ignored test", "this test does not count")
ignoreTest();
assertTrue(false);
endTest()

startTest("error", "test raising an error")
throw "does not work"
endTest()
