/*******************************************************************************
 * Copyright (c) 2019 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/
loadModule("/System/UI Builder");
loadModule("/System/UI");
loadModule("/Charting");

createView("Function Plot")

createLabel("Function:", "1/1 > x");
var txtFunction = createText("2-6/1 o!");
createButton("Plot", plotFunction, "7/1 o")

createLabel("Range:", "1/2 > x")
var txtRangeFrom = createText("2/2")
createLabel("to", "3/2 < x")
var txtRangeTo = createText("4/2")
createLabel("Step size", "5/2 < x")
var txtRangeStep = createText("6/2")
createButton("Clear", "clear()", "7/2 o")

executeUI("txtRangeFrom.setText('-10')")
executeUI("txtRangeTo.setText('10')")
executeUI("txtRangeStep.setText('1')")

chart = createChart(getComposite());
chartComposite = addControl(chart, "1-7/3 o! o!")

function plotFunction() {
	var formula = executeUI("txtFunction.getText()");
	var rangeFrom = parseFloat(executeUI("txtRangeFrom.getText()"))
	var rangeTo = parseFloat(executeUI("txtRangeTo.getText()"))
	var rangeStep = parseFloat(executeUI("txtRangeStep.getText()"))

	series(formula);
	for (var x = rangeFrom; x <= rangeTo; x += rangeStep) {
		print("from " + x)
		y = eval("x=" + x + ";\n" + formula);
		plotPoint(x, y);
	}
}