/**
 * toolbar: Project Explorer
 * name: Create P
 * image: create_project.png
 */
loadModule('/System/Resources');
loadModule("/System/UI");

var projectName = showInputDialog("Please provide a new name for your project");
if (projectName != null)
	createProject(projectName)
