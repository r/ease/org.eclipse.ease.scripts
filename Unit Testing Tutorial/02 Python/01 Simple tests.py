###############################################################################
# Copyright (c) 2017 Christian Pontesegger and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     Christian Pontesegger - initial API and implementation
###############################################################################

loadModule("/Unittest");

startTest("valid", "simple valid test")
assertTrue(True)
endTest()

startTest("invalid", "simple fail test")
assertTrue(False)
endTest()

startTest("ignored", "do not run test")
ignoreTest("will ignore failures, but will still report errors")
assertTrue(False)
endTest();

startTest("timeout", "see if a test takes too long")
setTestTimeout(100);
start = java.lang.System.currentTimeMillis()
while (start + 200 > java.lang.System.currentTimeMillis()):
    pass
    
endTest();

startTest("error", "test raising an error")
# raise(BaseException("does not work"))
endTest()