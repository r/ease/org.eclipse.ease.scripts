/*******************************************************************************
 * Copyright (c) 2019 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/
loadModule("/System/UI Builder", false);
loadModule("/System/UI");
loadModule('/System/Resources');

createView("Create Contact");

createLabel("First Name:", "1/1 >x");
var txtFirstName = createText("2-4/1 o!");
createLabel("Last Name:", "1/2 >x");
var txtLastName = createText("2-4/2 o!");

createLabel("Phone:", "1/3 >x");
var txtPhone = createText("2-4/3 o!");

createLabel("ZIP Code:", "1/4 >x");
var txtZipCode = createText("2/4 o");

createLabel("Country:", "3/4 >x");
var cmbCountry = createComboViewer([ "Austria", "Germany", "India", "USA" ],
		null, "4/4 o")

createSeparator(true, "1-4/5 o")
createButton("Save", saveAddress, "4/6 >")

function saveAddress() {

	var address = new Object();
	address.lastName = executeUI("txtLastName.getText();") + "";
	address.firstName = executeUI("txtFirstName.getText();") + "";
	address.phone = executeUI("txtPhone.getText();") + "";
	address.zipCode = executeUI("txtZipCode.getText();") + "";
	address.country = executeUI("cmbCountry.getSelection().getFirstElement();")
			+ "";
	var data = JSON.stringify(address);

	writeFile("workspace://UI Builder/AddressBook/data/" + address.lastName + " "
			+ address.firstName + ".address", data);
}