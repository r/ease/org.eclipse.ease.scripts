loadModule("/Unittest");
loadModule("/I2C Communication")

slaveAddress = 0xC0;
register = 0x01;
dataToWrite = 0xFA;

startTest("raw communication")
sendI2CStart();
sendI2C(slaveAddress)
sendI2C(register)
sendI2C(dataToWrite)
sendI2CStop()

sendI2CStart();
sendI2C(slaveAddress & 0x01)
data = readI2C()

assertEquals(data, dataToWrite);
endTest()



startTest("raw communication")
writeRegister(register, dataToWrite);
data = readRegister(register);

assertEquals(data, dataToWrite);
endTest()



