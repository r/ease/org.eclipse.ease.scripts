/*******************************************************************************
 * Copyright (c) 2019 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/
loadModule("/System/UI Builder", false);
loadModule("/System/Resources", false);

createView("Project Info")
var viewer = createTreeViewer(getWorkspace().getProjects(), getChildren);
createViewerColumn(viewer, "Resource", new org.eclipse.ui.model.WorkbenchLabelProvider(), 4);
createViewerColumn(viewer, "Size", createLabelProvider(getResourceSize), 1);

createComparator(viewer, "return (getProviderElement() instanceof org.eclipse.core.resources.IContainer) ? 1: 2;")

function getChildren() {
	if (getProviderElement() instanceof org.eclipse.core.resources.IContainer)
		return getProviderElement().members();
	
	return null;
}

function getResourceSize() {
	print(getProviderElement())
	if (getProviderElement() instanceof org.eclipse.core.resources.IContainer)
		return getProviderElement().members().length;
	
	return getProviderElement().getRawLocation().toFile().length() + " bytes";
}



