function fetchData(address) {
	url = new java.net.URL(address)
	connection = url.openConnection()
	inputStream = connection.getInputStream()

	// read stream into a string using a helper class
	data = org.eclipse.ease.tools.StringTools.toString(inputStream);

	// sample data in case the service is offline
	// data = '{	   "dstOffset" : 0,	   "rawOffset" : -28800,	   "status" : "OK",	   "timeZoneId" : "America/Los_Angeles",	   "timeZoneName" : "Pacific Standard Time"	}';
	
	inputStream.close();
	
	return data;
}

loadModule("/Unittest");

startTest("Fetch JSON data");
data = fetchData("https://maps.googleapis.com/maps/api/timezone/json?location=39.6034810,-119.6822510&timestamp=1331161200");
jsonData = JSON.parse(data);
assertEquals("OK", jsonData["status"])
endTest();



