EASE Core
=========

This is the script example part of Eclipse's EASE project. It contains sample scripts and demos on how to use the available scripting engines provided by EASE.

For more information and important links, refer to the [EASE documentation page] [1] or the [EASE project overview page] [2].

License
-------

[Eclipse Public License (EPL) v2.0][3]

[1]: https://www.eclipse.org/ease/documentation/
[2]: https://www.eclipse.org/ease/
[3]: http://wiki.eclipse.org/EPL
